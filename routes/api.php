<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('option1')->group(function () {
    Route::get('tests', 'LaravelTestController@index');
    Route::get('test/{test}', 'LaravelTestController@show');
    Route::post('test/', 'LaravelTestController@store');
    Route::patch('test/{test}', 'LaravelTestController@update');
    Route::delete('test/{test}', 'LaravelTestController@destroy');
});

Route::prefix('option2')->group(function () {
    Route::get('investigations', 'InvestigationController@index');
    Route::get('investigation/{investigation}', 'InvestigationController@show');
    Route::post('investigation/', 'InvestigationController@store');
    Route::patch('investigation/{investigation}', 'InvestigationController@update');
});



