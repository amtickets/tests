<?php

namespace Database\Factories\Investigation;

use Carbon\Carbon;
use App\Models\Investigation\Investigation;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvestigationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Investigation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->citySuffix(),
            'kod_oferty' => $this->faker->randomNumber(5),
            'icd' => (string) $this->faker->randomNumber(6),
            'research_code' => $this->faker->randomNumber(3, true),
            'category_type' => mt_rand(1, 2),
            'description' => $this->faker->realText($maxNbChars = 900, $indexSize = 2),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'categories' => [],
        ];
    }
}
