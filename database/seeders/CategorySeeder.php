<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    private $table = 'categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->insert($this->getEntities());
    }

    private function getEntities()
    {
        $faker = Factory::create();

        $entities = [];
        for ($i = 0, $count = 50; $i < $count; ++$i) {
            $entities[] = [
                'title' => $faker->secondaryAddress(),
                'code' => $faker->randomNumber(3),
                'description' => $faker->realText($maxNbChars = 900, $indexSize = 2),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
        }

        return $entities;
    }
}
