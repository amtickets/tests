<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestCategorySeeder extends Seeder
{
    private $table = 'types_categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tests = DB::table('tests')->get();

        foreach ($tests as $test) {
            $categoryIds = $this->getCategoryIds(mt_rand(0, 4));
            foreach ($categoryIds as $categoryId) {
                DB::table($this->table)->insert([
                    'test_id' => $test->id,
                    'category_id' => $categoryId,
                ]);
            }
        }
    }

    public function getCategoryIds($limit)
    {
        return DB::table('categories')
            ->inRandomOrder()
            ->limit($limit)
            ->pluck('id');
    }
}
