<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Services\InvestigationCategoryFactory;

class InvestigationCategorySeeder extends Seeder
{
    private $faker;

    /**
     * @param $faker
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_a')->insert($this->getEntities('a'));
        DB::table('category_b')->insert($this->getEntities('b'));
        DB::table('category_c')->insert($this->getEntities('c'));
        DB::table('category_d')->insert($this->getEntities('d'));

        $investigations = DB::table('investigations')->inRandomOrder()->take(5)->get();
        $tableCategories = ['category_a', 'category_b', 'category_c', 'category_d'];
        foreach ($investigations as $investigation) {
            foreach ($tableCategories as $table) {
                $cnt = mt_rand(0, 3);
                if ($cnt) {
                    $categories = DB::table($table)->inRandomOrder($cnt)->take($cnt)->get();
                    foreach ($categories as $category) {
                        DB::table('investigations_categories')->insert([
                            'investigation_id' => $investigation->id,
                            'title' => $investigation->title . ' ' . $investigation->kod_oferty . ' ' . $category->id,
                            'entityable_id' => $category->id,
                            'entityable_type' => InvestigationCategoryFactory::getCategoryClass(last(explode('_', $table))),
                        ]);
                    }
                }
            }
        }
    }

    private function getEntities($param)
    {
        $entities = [];
        for ($i = 0, $count = 50; $i < $count; $i++) {
            $entity = [];
            switch ($param) {
                case 'a':
                    $entity = $this->getEntitiesA();
                    break;
                case 'b':
                    $entity = $this->getEntitiesB();
                    break;
                case 'c':
                    $entity = $this->getEntitiesC();
                    break;
                case 'd':
                    $entity = $this->getEntitiesD();
                    break;
            }

            $entities[] = array_merge($entity, [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        return $entities;
    }

    private function getEntitiesA()
    {
        return [
            'title' => $this->faker->secondaryAddress(),
            'name_type' => $this->faker->word,
            'class' => $this->faker->word,
            'trf_index' => $this->faker->randomNumber(3, true),
        ];
    }

    private function getEntitiesB()
    {
        return [
            'title' => $this->faker->secondaryAddress(),
            'name_type' => $this->faker->word,
            'class' => $this->faker->word,
        ];
    }

    private function getEntitiesC()
    {
        return [
            'title' => $this->faker->secondaryAddress(),
            'name_type' => $this->faker->word,
        ];
    }

    private function getEntitiesD()
    {
        return [
            'title' => $this->faker->secondaryAddress(),
        ];
    }
}
