<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InvestigationSeeder extends Seeder
{
    private $table = 'investigations';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->insert($this->getEntities());
    }

    private function getEntities()
    {
        $faker = Factory::create();

        $entities = [];
        for ($i = 0, $count = 10; $i < $count; ++$i) {
            $entities[] = [
                'title' => $faker->citySuffix(),
                'kod_oferty' => $faker->randomNumber(5),
                'icd' => $faker->randomNumber(6),
                'research_code' => $faker->randomNumber(2),
                'category_type' => mt_rand(1,2),
                'description' => $faker->realText($maxNbChars = 900, $indexSize = 2),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
        }

        return $entities;
    }
}
