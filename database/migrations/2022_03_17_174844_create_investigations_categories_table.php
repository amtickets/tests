<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestigationsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigations_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('investigation_id');
            $table->foreign('investigation_id')->references('id')->on('investigations');
            $table->string('title');
            $table->unsignedBigInteger('entityable_id')->index();
            $table->string('entityable_type')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigations_categories');
    }
}
