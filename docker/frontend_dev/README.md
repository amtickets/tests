### Running The Project
```
$ docker-compose --project-name="lms50" -f docker/frontend_dev/docker-compose.yml up -d
```
### Getting Into A Docker Container
```
docker exec -it <container_name> /bin/bash

# this command prevents strange behaviour of the bash inside a docker console
docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti <container_name>  bash
```
The *names of containers* are in the [docker-compose.yml](docker-compose.yml) file.
Php container name is lms50_php.
