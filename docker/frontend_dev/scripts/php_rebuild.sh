#!/bin/bash

cd ~/WebstormProjects/lms50
[ -f .env ] && rm .env
cp .env.dist .env
sed -i 's/APP_ENV=dev/APP_ENV=dev/g' .env
if [ -d "data" ]; then
rm -rf data
fi

docker exec -t --user dockeruser lms50_php sh -c "composer install"
docker exec -t --user dockeruser lms50_php sh -c "composer update dzensoft/scorm-bundle"
docker exec -t --user dockeruser lms50_php sh -c "bin/console doctrine:database:drop --force --if-exists"
docker exec -t --user dockeruser lms50_php sh -c "bin/console doctrine:database:create"
docker exec -t --user dockeruser lms50_php sh -c "bin/console doctrine:migrations:migrate --quiet"
docker exec -t --user dockeruser lms50_php sh -c "bin/console doctrine:fixtures:load --quiet"
docker exec -t --user dockeruser lms50_php sh -c "openssl genrsa -out config/jwt/private.pem  4096"
docker exec -t --user dockeruser lms50_php sh -c "openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem"
docker exec -t --user dockeruser lms50_php sh -c "bin/console cache:clear"
docker exec -t --user dockeruser lms50_php sh -c "bin/console assets:install public"
docker exec -t --user dockeruser lms50_php sh -c "bin/console cache:warmup"
