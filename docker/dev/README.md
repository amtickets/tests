### Installing Docker
```
$ sudo apt-get install docker docker-compose
$ sudo systemctl start docker
$ sudo systemctl enable docker
$ sudo groupadd docker
$ sudo usermod -aG docker $USER

reboot your system
```
### Running Project
```
Add 10.10.0.40 driverline.local into the host file (/etc/hosts in Linux) and run:

$ export UID
$ docker-compose --project-name="testapp" -f docker/dev/docker-compose.yml up -d
$ docker-compose --project-name="testapp" -f docker/dev/docker-compose.yml stop
```
### Getting Into A Docker Container
```
docker exec -it <container_name> /bin/bash
docker exec -it testapp_php /bin/bash
docker exec -it testapp_mysql /bin/bash

# this command prevents strange behaviour of the bash inside a docker console
docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti <container_name>  bash
```
The *names of containers* are in the [docker-compose.yml](docker-compose.yml) file.
Php container name is testapp_php.

### Setting Up XDebug
- In PhpStorm go to the **Settings/Languages&Frameworks/PHP/Server** 
- Create a new server with a name "testapp" and click on the checkbox **Use path mappings**
- In the area bellow under the caption Project Files set the **Absolute path on the server** to /var/www/testapp

### VPN (chrome browser for acceptance tests)
```
$ xtightvncviewer 10.10.0.5
```

### Supervisor web-monitor
http://10.10.0.20:9001<br>
username: user<br>
password: user

### Rabbit web-monitor
http://10.10.0.90:15672/<br>
username: guest<br>
password: guest<br>
