SHELL:=/bin/bash

start: ## Start docker
	export UID && \
	docker-compose --project-name="testapp" -f docker/dev/docker-compose.yml up -d

stop: ## Stop docker
	export UID && \
	docker-compose --project-name="testapp" -f docker/dev/docker-compose.yml stop

in: ## In container
	docker exec -it testapp_php /bin/bash

update: ## Git update
	cd /home/linker/projects/smp && \
	git pull && \
	cd /home/linker/projects/core && \
	git pull

help: ## Shows help dialog.
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//'`); \
	for help_line in $${help_lines[@]}; do \
		IFS=$$'#' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf "%-30s %s\n" $$help_command $$help_info ; \
	done
