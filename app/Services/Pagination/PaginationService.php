<?php

namespace App\Services\Pagination;

class PaginationService
{
    /** @var array */
    private $default = [];

    private $sortFields = [];

    private $sortDirections = [];

    private $sortPages = [];

    private $defaultSortPages = [10, 50, 100];

    private $sortBy;

    private $orderBy;

    private $perPage;

    public function __construct(array $default, array $sortFields, array $sortPages = [])
    {
        $this->default = $default;
        $this->sortFields = $sortFields;
        $this->sortDirections = ['asc', 'desc'];
        $this->sortPages = count($sortPages) ? $sortPages : $this->defaultSortPages;
    }

    public function addPagination($request, $query)
    {
        $this->validate($request);

        return $query
            ->orderBy($this->sortBy, $this->orderBy)
            ->paginate($this->perPage);
    }

    public function validate($request)
    {
        $this->sortBy = $this->getRequestParam($request->sort_by, $this->sortFields, $this->default['sort_by']);
        $this->orderBy = $this->getRequestParam($request->order_by, $this->sortDirections, $this->default['order_by']);
        $this->perPage = $this->getRequestParam($request->per_page, $this->sortPages, $this->default['per_page']);
    }

    /**
     * @param $param
     * @param array $array
     * @param $default
     *
     * @return mixed
     */
    private function getRequestParam($param, array $haystack, $default)
    {
        if (!in_array($param, $haystack)) {
            return $default;
        }

        return $param;
    }
}
