<?php

namespace App\Services;

use App\Models\Investigation\CategoryA;
use App\Models\Investigation\CategoryB;
use App\Models\Investigation\CategoryC;
use App\Models\Investigation\CategoryD;
use App\Http\Resources\Investigation\CategoryAResource;
use App\Http\Resources\Investigation\CategoryBResource;
use App\Http\Resources\Investigation\CategoryCResource;
use App\Http\Resources\Investigation\CategoryDResource;

class InvestigationCategoryFactory
{
    /**
     * @param string $type
     *
     * @return string
     */
    public static function getCategoryClass(string $type)
    {
        switch (mb_strtolower($type)) {
            case 'a':
                return CategoryA::class;
            case 'b':
                return CategoryB::class;
            case 'c':
                return CategoryC::class;
            default:
                return CategoryD::class;
        }
    }

    /**
     * @param string $entityType
     * @param object $entityable
     *
     * @return CategoryAResource|CategoryBResource|CategoryCResource|CategoryDResource
     */
    public static function getCategoryResource(string $entityType, object $entityable)
    {
        switch ($entityType) {
            case CategoryA::class:
                return new CategoryAResource($entityable);
            case CategoryB::class:
                return new CategoryBResource($entityable);
            case CategoryC::class:
                return new CategoryCResource($entityable);
            default:
                return new CategoryDResource($entityable);
        }
    }
}
