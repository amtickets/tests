<?php

namespace App\Services;

class Word
{
    /**
     * @param array $array
     * @return mixed
     */
    public static function getRandomWord(array $array)
    {
        $rand = mt_rand(0, count($array) - 1);

        return $array[$rand];
    }
}
