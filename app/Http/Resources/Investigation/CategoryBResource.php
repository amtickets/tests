<?php

namespace App\Http\Resources\Investigation;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryBResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'name_type' => $this->name_type,
            'class' => $this->class,
            'type' => 'CategoryB',
            'created_at' => $this->created_at,
        ];
    }
}
