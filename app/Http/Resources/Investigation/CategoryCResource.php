<?php

namespace App\Http\Resources\Investigation;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryCResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'name_type' => $this->name_type,
            'type' => 'CategoryC',
            'created_at' => $this->created_at,
        ];
    }
}
