<?php

namespace App\Http\Resources\Investigation;

use Illuminate\Http\Resources\Json\JsonResource;

class InvestigationResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'kod_oferty' => $this->kod_oferty,
            'icd' => $this->icd,
            'research_code' => $this->research_code,
            'category_type' => $this->category_type,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'categories' => InvestigationCategoryResource::collection($this->investigationCategories),
        ];
    }
}
