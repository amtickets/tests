<?php

namespace App\Http\Resources\Investigation;

use App\Services\InvestigationCategoryFactory;
use Illuminate\Http\Resources\Json\JsonResource;

class InvestigationCategoryResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param $request
     *
     * @return CategoryAResource|CategoryBResource|CategoryCResource|CategoryDResource
     */
    public function toArray($request)
    {
        return InvestigationCategoryFactory::getCategoryResource($this->entityable_type, $this->entityable);
    }
}
