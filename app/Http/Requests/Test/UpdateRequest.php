<?php

namespace App\Http\Requests\Test;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:100',
            'kod_oferty' => 'required|integer|min:1|max:10000000',
            'description' => 'nullable|string|max:1200',
            'types_ids' =>  'present|array',
            'types_ids.*' => 'integer',
        ];
    }
}
