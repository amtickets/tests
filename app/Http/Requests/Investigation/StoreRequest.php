<?php

namespace App\Http\Requests\Investigation;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:100',
            'kod_oferty' => 'required|integer|min:1|max:10000000',
            'icd' =>  'required|string|max:10',
            'research_code' => 'required|integer|min:100|max:999',
            'description' => 'nullable|string|max:1200',
            'category_type' => 'required|integer|in:1,2',
            'categories' =>  'present|array',
            'categories.*.class' => 'required|string|max:1',
            'categories.*.id' => 'required|integer|min:1',
        ];
    }
}
