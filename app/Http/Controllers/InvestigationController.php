<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Investigation\Investigation;
use App\Services\InvestigationCategoryFactory;
use App\Services\Pagination\PaginationService;
use App\Http\Requests\Investigation\StoreRequest;
use App\Http\Requests\Investigation\UpdateRequest;
use App\Models\Investigation\InvestigationCategory;
use App\Http\Resources\Investigation\InvestigationResource;

class InvestigationController extends Controller
{
    /**
     * A list of investigations.
     *
     * @OA\Get(
     *  path="/api/option2/investigations",
     *  security={{"bearerAuth": {}}},
     *  tags={"Investigation"},
     *
     * @OA\Parameter(
     *      name="class",
     *      in="query",
     *      description="The class for modelr.",
     *      example="A",
     *      required=false
     *  ),
     * @OA\Parameter(
     *      name="types[0]",
     *      in="query",
     *      description="The type identifier.",
     *      example="1",
     *      required=false
     *  ),
     *
     * @OA\Response(response="200",     description="OK", @OA\JsonContent()),
     * @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query = Investigation::select('investigations.*')
            ->with(['investigationCategories.entityable']);

        if ($request->class !== null && $request->types && is_array($request->types) && count($request->types)) {
            $query->join(
                'investigations_categories',
                'investigations_categories.investigation_id',
                '=',
                'investigations.id'
            );

            $query
                ->whereIn('investigations_categories.entityable_id', $request->types)
                ->where(
                    'investigations_categories.entityable_type',
                    InvestigationCategoryFactory::getCategoryClass($request->class)
                )
                ->distinct();
        }
        $paginationService = new PaginationService(
            Investigation::SORT_DEFAULT,
            Investigation::SORT_FIELDS,
            Investigation::SORT_PAGES
        );
        $investigations = $paginationService->addPagination($request, $query);

        return InvestigationResource::collection($investigations);
    }

    /**
     * View an investigation.
     *
     * @OA\Get(
     *  path="/api/option2/investigation/{investigation}",
     *  security={{"bearerAuth": {}}},
     *  tags={"Investigation"},
     *
     * @OA\Parameter(name="Investigation", in="path", description="The investigation id.", example=1, required=true),
     *
     * @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *
     * @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @param Investigation $investigation
     *
     * @return InvestigationResource
     */
    public function show(Investigation $investigation)
    {
        return new InvestigationResource($investigation);
    }

    /**
     * Create a investigation in any front.
     *
     * @OA\Post(
     *  path="/api/option2/investigation",
     *  security={{"bearerAuth": {}}},
     *  tags={"Investigation"},
     *
     * @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *
     * @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @param StoreRequest $request
     *
     * @return InvestigationResource
     */
    public function store(StoreRequest $request)
    {
        /**
         * @var Investigation $investigation
         */
        $investigation = Investigation::create([
            'title' => $request->title,
            'kod_oferty' => $request->kod_oferty,
            'icd' => $request->icd,
            'research_code' => $request->research_code,
            'description' => $request->description,
            'category_type' => $request->category_type,
        ]);

        InvestigationCategory::addAllCategories($investigation, $request->categories);

        return new InvestigationResource($investigation);
    }

    /**
     * Update a investigation in any front.
     *
     * @OA\Patch(
     *  path="/api/option2/investigation/{investigation}",
     *  security={{"bearerAuth": {}}},
     *  tags={"Investigation"},
     *
     * @OA\Parameter(name="Investigation", in="path", description="The investigation id.", example=1, required=true),
     *
     * @OA\Response(response="200",     description="OK", @OA\JsonContent()),
     * @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @param UpdateRequest $request
     * @param Investigation $investigation
     *
     * @return InvestigationResource
     */
    public function update(UpdateRequest $request, Investigation $investigation)
    {
        $investigation->update([
            'title' => $request->title,
            'kod_oferty' => $request->kod_oferty,
            'icd' => $request->icd,
        ]);

        $investigation->investigationCategories()->delete();
        InvestigationCategory::addAllCategories($investigation, $request->categories);

        return new InvestigationResource($investigation);
    }
}
