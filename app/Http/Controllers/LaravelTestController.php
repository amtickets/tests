<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;
use App\Http\Resources\TestResource;
use App\Http\Requests\Test\StoreRequest;
use App\Http\Requests\Test\UpdateRequest;
use App\Services\Pagination\PaginationService;

class LaravelTestController extends Controller
{
    /**
     * A list of tests.
     *
     * @OA\Get(
     *  path="/api/option1/test",
     *  security={{"bearerAuth": {}}},
     *  tags={"Test"},
     *
     *  @OA\Parameter(
     *      name="types[0]",
     *      in="query",
     *      description="The type identifier.",
     *      example="1",
     *      required=false
     *  ),
     *
     *  @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *  @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query = Test::select('tests.*')
            ->with(['testTypes'])
            ->leftjoin('types_categories', 'types_categories.test_id', '=', 'tests.id')
            ->filter($request);

        $paginationService = new PaginationService(Test::SORT_DEFAULT, Test::SORT_FIELDS);
        $tests = $paginationService->addPagination($request, $query);

        return TestResource::collection($tests);
    }

    /**
     * View a test.
     *
     * @OA\Get(
     *  path="/api/option1/test/{test}",
     *  security={{"bearerAuth": {}}},
     *  tags={"Test"},
     *
     *  @OA\Parameter(name="test", in="path", description="The test id.", example=1, required=true),
     *
     *  @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *  @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @param Test $test
     *
     * @return TestResource
     */
    public function show(Test $test)
    {
        return new TestResource($test);
    }

    /**
     * Create a test in any front.
     *
     * @OA\Post(
     *  path="/api/option1/test",
     *  security={{"bearerAuth": {}}},
     *  tags={"Test"},
     *
     *  @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *  @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @param StoreRequest $request
     *
     * @return TestResource
     */
    public function store(StoreRequest $request)
    {
        /** @var Test $test */
        $test = Test::create([
            'title' => $request->title,
            'kod_oferty' => $request->kod_oferty,
            'icd' => $request->icd,
            'research_code' => $request->research_code,
            'description' => $request->description,
            'category_type' => $request->category_type,
        ]);

        $test->addTypes($request->types_ids);

        return new TestResource($test);
    }

    /**
     * Update a test in any front.
     *
     * @OA\Patch(
     *  path="/api/option1/test/{test}",
     *  security={{"bearerAuth": {}}},
     *  tags={"Test"},
     *
     *  @OA\Parameter(name="test", in="path", description="The test id.", example=1, required=true),
     *
     *  @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *  @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     *
     * @param UpdateRequest $request
     * @param Test          $test
     *
     * @return TestResource
     */
    public function update(UpdateRequest $request, Test $test)
    {
        $test->update([
            'title' => $request->title,
            'kod_oferty' => $request->kod_oferty,
            'description' => $request->description,
        ]);
        $test->addTypes($request->types_ids);

        return new TestResource($test);
    }

    /**
     * Remove test.
     *
     * @OA\Delete(
     *  path="/api/option1/test/{id}",
     *  security={{"bearerAuth": {}}},
     *  tags={"Test"},
     *
     *  @OA\Parameter(name="id", in="path", description="The test identifier.", example=1, required=true),
     *
     *  @OA\Response(response="200", description="OK", @OA\JsonContent()),
     *  @OA\Response(response="default", description="Error", @OA\JsonContent()),
     * )
     */
    public function destroy(Test $test)
    {
        try {
            $test->testTypes()->detach();
            $test->delete();

            return response()->json(['status' => 'success'], 200);
        } catch (\Exception $e) {
            report($e);
        }
    }
}
