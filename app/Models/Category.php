<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    public const TYPE_ID = 1;

    protected $fillable = [
        'title', 'description',
    ];

    public function tests()
    {
        return $this->belongsToMany(Test::class, 'types_categories');
    }
}
