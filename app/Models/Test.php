<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Test extends Model
{
    use HasFactory;

    protected $table = 'tests';

    protected $fillable = [
        'title', 'kod_oferty', 'icd', 'research_code', 'description', 'category_type',
    ];

    public const SORT_DEFAULT = [
        'sort_by' => 'id',
        'order_by' => 'desc',
        'per_page' => 10,
    ];

    public const SORT_FIELDS = [
        'id', 'title', 'kod_oferty', 'icd', 'research_code',
    ];

    public function testTypes()
    {
        return $this->belongsToMany(Category::class, 'types_categories');
    }

    public function addTypes($typesIds)
    {
        if (count($typesIds)) {
            $typesIds = Category::whereIn('id', $typesIds)->pluck('id');
        }
        $this->testTypes()->sync($typesIds);
    }

    public function scopeFilter($query, $request)
    {
        if ($request->types && is_array($request->types) && count($request->types)) {
            $query->whereIn('types_categories.category_id', $request->types);
        }
    }
}
