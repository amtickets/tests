<?php

namespace App\Models\Investigation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Investigation extends Model
{
    use HasFactory;

    protected $table = 'investigations';

    public const SORT_DEFAULT = [
        'sort_by' => 'id',
        'order_by' => 'desc',
        'per_page' => 5,
    ];

    public const SORT_FIELDS = [
        'id', 'title', 'kod_oferty', 'icd', 'research_code',
    ];

    public const SORT_PAGES = [5, 10, 20];

    protected $fillable = [
        'title', 'kod_oferty', 'icd', 'research_code', 'description', 'category_type',
    ];

    public function investigationCategories()
    {
        return $this->hasMany(InvestigationCategory::class);
    }
}
