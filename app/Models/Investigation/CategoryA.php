<?php

namespace App\Models\Investigation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryA extends Model
{
    use HasFactory;

    protected $table = 'category_a';

    protected $fillable = [
        'title', 'name_type', 'class', 'trf_index',
    ];
}
