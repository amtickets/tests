<?php

namespace App\Models\Investigation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoryD extends Model
{
    use HasFactory;

    protected $table = 'category_d';
}
