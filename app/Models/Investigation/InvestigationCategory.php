<?php

namespace App\Models\Investigation;

use Illuminate\Database\Eloquent\Model;
use App\Services\InvestigationCategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class InvestigationCategory extends Model
{
    use HasFactory;

    protected $table = 'investigations_categories';

    protected $fillable = [
        'title', 'investigation_id',
    ];

    /**
     * Get polymorphic model.
     */
    public function entityable()
    {
        return $this->morphTo();
    }

    /**
     * @param Investigation $investigation
     * @param array         $categories
     *
     * @return void
     */
    public static function addAllCategories(Investigation $investigation, array $categories)
    {
        foreach ($categories as $category) {
            $class = InvestigationCategoryFactory::getCategoryClass($category['class']);
            $objectCategory = $class::find($category['id']);
            if ($objectCategory) {
                $nvestigationCategory = new self();
                $nvestigationCategory->saveMorthCategory([
                    'title' => $investigation->title . ' ' . $investigation->kod_oferty . ' ' . $objectCategory->id,
                    'investigation_id' => $investigation->id,
                ], $objectCategory);
            }
        }
    }

    /**
     * @param array $data
     * @param $entity
     *
     * @return bool
     */
    private function saveMorthCategory(array $data, $entity)
    {
        $this->fill($data);
        $this->entityable_id = $entity->id;
        $this->entityable_type = get_class($entity);

        return $this->save();
    }
}
