<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use App\Models\Investigation\Investigation;
use Illuminate\Testing\Fluent\AssertableJson;

class InvestigationControllerTest extends TestCase
{
    /**
     * @test store entity without categories
     */
    public function storeInvestigationWithoutCategories()
    {
        $investigation = $this->getEntity();

        // add investigation entity
        $response = $this->postJson('api/option2/investigation', $investigation->toArray())
            ->assertStatus(201);

        // check JSON response
        $response->assertJsonStructure([
            'data' => ['id', 'title', 'kod_oferty', 'icd', 'research_code', 'category_type', 'description'],
        ]);

        // quantity of categories = 0
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 0)
        );
    }

    /**
     * @test store entity with four categories of class A
     */
    public function storeInvestigationWith4ACategories()
    {
        $investigation = $this->getEntity();
        $data = $investigation->toArray();
        $data['categories'] = $this->prepareCategories(4, 'a');

        // add investigation entity
        $response = $this->postJson('api/option2/investigation', $data)->assertStatus(201);

        // check JSON response for A category
        $response->assertJsonStructure([
            'data' => [
                'categories' => [
                    '*' => ['id', 'title', 'name_type', 'class', 'trf_index', 'type'],
                ],
            ],
        ]);

        // quantity of categories = 4
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 4)
        );
    }

    /**
     * @test store entity with two categories of class B
     */
    public function storeInvestigationWith2BCategories()
    {
        $investigation = $this->getEntity();
        $data = $investigation->toArray();
        $data['categories'] = $this->prepareCategories(2, 'b');

        // add investigation entity
        $response = $this->postJson('api/option2/investigation', $data)->assertStatus(201);

        // check JSON response for B category
        $response->assertJsonStructure([
            'data' => [
                'categories' => [
                    '*' => ['id', 'title', 'name_type', 'class', 'type'],
                ],
            ],
        ]);

        // quantity of categories = 2
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 2)
        );
    }

    /**
     * @test store entity with five categories of class C
     */
    public function storeInvestigationWith5CCategories()
    {
        $investigation = $this->getEntity();
        $data = $investigation->toArray();
        $data['categories'] = $this->prepareCategories(5, 'c');

        // add investigation entity
        $response = $this->postJson('api/option2/investigation', $data)->assertStatus(201);

        // check JSON response for C category
        $response->assertJsonStructure([
            'data' => [
                'categories' => [
                    '*' => ['id', 'title', 'name_type', 'type'],
                ],
            ],
        ]);

        // quantity of categories = 5
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 5)
        );
    }

    /**
     * @test store entity with two categories of class D
     */
    public function storeInvestigationWith2DCategories()
    {
        $investigation = $this->getEntity();
        $data = $investigation->toArray();
        $data['categories'] = $this->prepareCategories(2, 'd');

        // add investigation entity
        $response = $this->postJson('api/option2/investigation', $data)->assertStatus(201);

        // check JSON response for D category
        $response->assertJsonStructure([
            'data' => [
                'categories' => [
                    '*' => ['id', 'title', 'type'],
                ],
            ],
        ]);

        // quantity of categories = 2
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 2)
        );
    }

    /**
     * @test get investigation entity
     */
    public function getInvestigation()
    {
        $investigation = $this->getEntityFromDB();

        // get entity
        $this->getJson('api/option2/investigation/' . $investigation->id)
            ->assertStatus(200)
            ->assertJsonPath('data.id', $investigation->id);
    }

    /**
     * @test get ten investigations from database
     */
    public function get10Investigations()
    {
        $params = [
            'per_page' => 10,
        ];

        // get 10 entities
        $response = $this->getJson('api/option2/investigations?' . http_build_query($params))
            ->assertStatus(200);

        // check JSON response for All categories
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'categories' => [
                        '*' => ['id', 'title', 'type'],
                    ],
                ],
            ],
        ]);

        // quantity entities = 10
        $result = $response->getOriginalContent();
        $this->assertEquals(count($result), 10);
    }

    /**
     * @test update investigation add zero categories
     */
    public function updateInvestigationAdd0Categories()
    {
        $investigation = $this->getEntityFromDB();
        // fill fields for updating investigation
        $investigation->title = 'Old title_';
        $investigation->kod_oferty = 752;
        $investigation->description = 'Old description_';
        $investigation->icd = 'T1000';

        // we can not update research code !!!
        $oldResearchCode = $investigation->research_code;
        $investigation->research_code = 347;

        $data = (array) $investigation;
        $data['categories'] = [];

        $response = $this->patchJson('api/option2/investigation/' . $investigation->id, $data)
            ->assertStatus(200);

        $result = $response->getOriginalContent();

        // check fields
        $this->assertEquals($result->id, $data['id']);
        $this->assertEquals($result->title, $data['title']);
        $this->assertEquals($result->kod_oferty, $data['kod_oferty']);
        $this->assertEquals($result->icd, $data['icd']);

        // check research_code field
        $this->assertNotEquals($result->research_code, $data['research_code']);
        $this->assertEquals($result->research_code, $oldResearchCode);

        // quantity of categories = 0
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 0)
        );
    }

    /**
     * @test update investigation add two B categories
     */
    public function updateInvestigationWith2BCategories()
    {
        $investigation = $this->getEntityFromDB();
        $data = (array) $investigation;
        $data['categories'] = $this->prepareCategories(2, 'b');

        $response = $this->patchJson('api/option2/investigation/' . $investigation->id, $data)
            ->assertStatus(200);

        // check JSON response for B category
        $response->assertJsonStructure([
            'data' => [
                'categories' => [
                    '*' => ['id', 'title', 'name_type', 'class', 'type'],
                ],
            ],
        ]);

        // quantity of categories = 3
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.categories', 2)
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private function getEntity()
    {
        return Investigation::factory()->make();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function getEntityFromDB()
    {
        return DB::table('investigations')->inRandomOrder()->first();
    }

    /**
     * @param int    $limit
     * @param string $classCategory
     *
     * @return array
     */
    private function prepareCategories(int $limit, string $classCategory)
    {
        $data = [];
        $categoryIds = DB::table('category_' . strtolower($classCategory))
            ->inRandomOrder()->take($limit)->pluck('id');

        foreach ($categoryIds as $id) {
            $data[] = ['class' => strtoupper($classCategory), 'id' => $id];
        }

        return $data;
    }
}
