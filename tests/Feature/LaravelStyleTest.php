<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Test;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Testing\Fluent\AssertableJson;

use function PHPUnit\Framework\assertEquals;

class LaravelStyleTest extends TestCase
{
    /**
     * @test store test entity without categories
     */
    public function storeTestEntity0Types()
    {
        $test = $this->getEntity();
        $response = $this->postJson('api/option1/test', $test->toArray())->assertStatus(201);

        // check JSON response
        $response->assertJsonStructure([
            'data' => ['id', 'title', 'kod_oferty', 'icd', 'research_code', 'description', 'types'],
        ]);

        // quantity of categories = 0
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.types', 0)
        );
    }

    /**
     * @test store test entity with two categories
     */
    public function storeTestEntity2Types()
    {
        $test = $this->getEntity();
        $data = $test->toArray();
        $data['types_ids'] = $this->getCategoryIdsFromDB(2);

        $response = $this->postJson('api/option1/test', $data)->assertStatus(201);

        // quantity of categories = 2
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.types', 2)
        );
    }

    /**
     * @test store test entity without categories
     */
    public function storeTestEntityWrongKodOferty()
    {
        $test = $this->getEntity();
        $test->kod_oferty = '124213123';

        $response = $this->postJson('api/option1/test', $test->toArray())->assertStatus(422);

        // check JSON error structure response
        $response->assertJsonStructure(['errors' => ['kod_oferty']]);
    }

    /**
     * @test get test entity without categories
     */
    public function getTestEntity()
    {
        $test = $this->getEntityFromDB();

        $response = $this->getJson('api/option1/test/' . $test->id)->assertStatus(200);

        // check JSON response
        $response->assertJsonStructure([
            'data' => ['id', 'title', 'kod_oferty', 'icd', 'research_code', 'description', 'types'],
        ]);
    }

    /**
     * @test get test entities with filter by category
     */
    public function getTestEntitities1Category()
    {
        $typeCategory = DB::table('types_categories')
            ->inRandomOrder()->take(1)->pluck('category_id')
            ->first();

        $response = $this->getJson('api/option1/tests?' . http_build_query([
            'types[0]' => $typeCategory,
            'per_page' => 50,
            'order_by' => 'desc',
        ]))->assertStatus(200);

        $response->assertJsonFragment(['id' => $typeCategory]);
    }

    /**
     * @test update test entity
     */
    public function updateTestEntity()
    {
        $test = $this->getEntityFromDB();
        $test->title = 'New title';
        $test->kod_oferty = 546;
        $test->description = 'New description';
        // we can not update icd !!!
        $oldIsd = $test->icd;
        $test->icd = 'VV124';

        $data = (array) $test;
        $data['types_ids'] = [];

        $response = $this->patchJson('api/option1/test/' . $test->id, $data)->assertStatus(200);

        // check fields
        $response->assertJsonPath('data.title', $data['title'])
            ->assertJsonPath('data.kod_oferty', $data['kod_oferty'])
            ->assertJsonPath('data.description', $data['description'])
            ->assertJsonPath('data.icd', $oldIsd);

        // quantity of categories = 0
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.types', 0)
        );
    }

    /**
     * @test update test entity with 5 categories
     */
    public function updateTestEntity3Categories()
    {
        $test = $this->getEntityFromDB();

        $data = (array) $test;
        $data['types_ids'] = $this->getCategoryIdsFromDB(3);

        $response = $this->patchJson('api/option1/test/' . $test->id, $data)->assertStatus(200);

        // check JSON response
        $response->assertJsonStructure([
            'data' => [
                'types' => [
                    '*' => ['id', 'title', 'code', 'description'],
                ],
            ],
        ]);

        // quantity of categories = 3
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.types', 3)
        );
    }

    /**
     * @test delete test entity
     */
    public function deleteTestEntity()
    {
        $test = $this->getEntityFromDB();
        $this->deleteJson('api/option1/test/' . $test->id, (array) $test)->assertStatus(200);

        // search for deleted test in DB
        $test = DB::table('tests')->find($test->id);
        assertEquals($test, null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private function getEntity()
    {
        return Test::factory()->make();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function getEntityFromDB()
    {
        return DB::table('tests')->inRandomOrder()->first();
    }

    /**
     * @param int $take
     *
     * @return \Illuminate\Support\Collection
     */
    private function getCategoryIdsFromDB(int $take)
    {
        return DB::table('categories')->inRandomOrder()->take($take)->pluck('id');
    }
}
