<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Test;
use Illuminate\Support\Facades\DB;

class LaravelStyleModelTest extends TestCase
{
    /**
     * @test add zero types to entity
     * addTypes
     */
    public function addZeroTypesToTest()
    {
        $test = $this->getEntity();
        // save to database
        $test = Test::create($test->toArray());

        $test->addTypes([]);
        $this->assertEquals(0, $test->testTypes()->count());
    }

    /**
     * @test add two types to entity
     */
    public function addTwTypesToTest()
    {
        $test = $this->getEntity();
        // save to database
        $test = Test::create($test->toArray());

        $countTypes = 2;
        $typesIds = DB::table('categories')->inRandomOrder()->take($countTypes)->pluck('id');
        $test->addTypes($typesIds);

        $this->assertEquals($countTypes, $test->testTypes()->count());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private function getEntity()
    {
        return Test::factory()->make();
    }
}
