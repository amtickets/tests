<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Investigation\CategoryA;
use App\Models\Investigation\CategoryB;
use App\Models\Investigation\CategoryC;
use App\Models\Investigation\CategoryD;
use App\Services\InvestigationCategoryFactory;
use App\Models\Investigation\InvestigationCategory;

class InvestigationCategoryFactoryTest extends TestCase
{
    /**
     * @test get resource two categories for class A
     */
    public function getResourceFor2CategoriesClassA()
    {
        $class = CategoryA::class;
        $investigationCategories = $this->getInvestigationCategories($class, 2);
        $this->assertEquals(count($investigationCategories), 2);

        foreach ($investigationCategories as $investigationCategory) {
            $response = InvestigationCategoryFactory::getCategoryResource($class, $investigationCategory->entityable);
            $this->assertEquals($investigationCategory->entityable_id, $response->id);

            $arrayResponse = $response->jsonSerialize();
            // check JSON fields for A category
            $this->assertTrue(array_key_exists('title', $arrayResponse));
            $this->assertTrue(array_key_exists('name_type', $arrayResponse));
            $this->assertTrue(array_key_exists('class', $arrayResponse));
            $this->assertTrue(array_key_exists('trf_index', $arrayResponse));
        }
    }

    /**
     * @test get resource three categories for class B
     */
    public function getResourceFor3CategoriesClassB()
    {
        $class = CategoryB::class;
        $investigationCategories = $this->getInvestigationCategories($class, 3);
        $this->assertEquals(count($investigationCategories), 3);

        foreach ($investigationCategories as $investigationCategory) {
            $response = InvestigationCategoryFactory::getCategoryResource($class, $investigationCategory->entityable);
            $this->assertEquals($investigationCategory->entityable_id, $response->id);

            $arrayResponse = $response->jsonSerialize();
            // check JSON fields for B category
            $this->assertTrue(array_key_exists('title', $arrayResponse));
            $this->assertTrue(array_key_exists('name_type', $arrayResponse));
            $this->assertTrue(array_key_exists('class', $arrayResponse));
            $this->assertNotTrue(array_key_exists('trf_index', $arrayResponse));
        }
    }

    /**
     * @test get resource four categories for class C
     */
    public function getResourceFor4CategoriesClassC()
    {
        $class = CategoryC::class;
        $investigationCategories = $this->getInvestigationCategories($class, 4);
        $this->assertEquals(count($investigationCategories), 4);

        foreach ($investigationCategories as $investigationCategory) {
            $response = InvestigationCategoryFactory::getCategoryResource($class, $investigationCategory->entityable);
            $this->assertEquals($investigationCategory->entityable_id, $response->id);

            $arrayResponse = $response->jsonSerialize();
            // check JSON fields for C category
            $this->assertTrue(array_key_exists('title', $arrayResponse));
            $this->assertTrue(array_key_exists('name_type', $arrayResponse));
            $this->assertNotTrue(array_key_exists('class', $arrayResponse));
            $this->assertNotTrue(array_key_exists('trf_index', $arrayResponse));
        }
    }

    /**
     * @test get resource five categories for class D
     */
    public function getResourceFor5CategoriesClassD()
    {
        $class = CategoryD::class;
        $investigationCategories = $this->getInvestigationCategories($class, 5);
        $this->assertEquals(count($investigationCategories), 5);

        foreach ($investigationCategories as $investigationCategory) {
            $response = InvestigationCategoryFactory::getCategoryResource($class, $investigationCategory->entityable);
            $this->assertEquals($investigationCategory->entityable_id, $response->id);

            $arrayResponse = $response->jsonSerialize();
            // check JSON fields for D category
            $this->assertTrue(array_key_exists('title', $arrayResponse));
            $this->assertNotTrue(array_key_exists('name_type', $arrayResponse));
            $this->assertNotTrue(array_key_exists('class', $arrayResponse));
            $this->assertNotTrue(array_key_exists('trf_index', $arrayResponse));
        }
    }

    /**
     * @param $class
     * @param $take
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getInvestigationCategories($class, $take)
    {
        return InvestigationCategory::with(['entityable'])
            ->where('entityable_type', $class)
            ->inRandomOrder()
            ->take($take)
            ->get();
    }
}
