<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Investigation\Investigation;
use App\Models\Investigation\InvestigationCategory;

class InvestigationCategoryTest extends TestCase
{
    /**
     * @test add new 5 different categories to investigation
     */
    public function syncFiveCategoriesToInvestigation()
    {
        $investigation = Investigation::inRandomOrder()->first();
        $categories = [
            ['class' => 'a', 'id' => 2],
            ['class' => 'b', 'id' => 6],
            ['class' => 'c', 'id' => 10],
            ['class' => 'd', 'id' => 7],
            ['class' => 'd', 'id' => 4],
        ];

        // delete old categories
        $investigation->investigationCategories()->delete();
        // add new 5 categories
        InvestigationCategory::addAllCategories($investigation, $categories);
        $this->assertEquals($investigation->investigationCategories()->count(), count($categories));
    }

    /**
     * @test attach 3 different categories to investigation
     */
    public function attachThreeCategoriesToInvestigation()
    {
        $investigationCategory = InvestigationCategory::inRandomOrder()->first();
        // find investigation with categories
        $investigation = Investigation::inRandomOrder()->find($investigationCategory->investigation_id);
        $oldCountCategories = $investigation->investigationCategories()->count();
        $categories = [
            ['class' => 'a', 'id' => 2],
            ['class' => 'c', 'id' => 3],
            ['class' => 'c', 'id' => 5],
        ];
        // add new 3 categories
        InvestigationCategory::addAllCategories($investigation, $categories);

        $this->assertEquals(
            $investigation->investigationCategories()->count(),
            $oldCountCategories + count($categories)
        );
    }
}
